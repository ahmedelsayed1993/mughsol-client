package com.aait.maghsoolclient.Cart

import androidx.room.*

@Entity(indices = [Index(value = ["product_id"], unique = false)])

class ProviderModelOffline(var provider_id: Int,
                           var provider_name: String?,
                           var provider_image: String?,
                           var address:String?,
                           var subcategory:Int?,
                           var name:String?,
                           var product_id: Int,
                           var product_name: String?,
                           var product_image: String?,
                           var price:String?,
                           var count:Int?
) {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}