package com.aait.maghsoolclient.UI.Fragments.Client

import android.os.Bundle
import android.view.View
import com.aait.maghsoolclient.Base.BaseFragment
import com.aait.maghsoolclient.R

class ProfileFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_profile
    companion object {
        fun newInstance(): ProfileFragment {
            val args = Bundle()
            val fragment = ProfileFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    override fun initializeComponents(view: View) {

    }
}