package com.aait.maghsoolclient.UI.Fragments.Client

import android.os.Bundle
import android.view.View
import com.aait.maghsoolclient.Base.BaseFragment
import com.aait.maghsoolclient.R

class OrdersFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_orders
    companion object {
        fun newInstance(): OrdersFragment {
            val args = Bundle()
            val fragment = OrdersFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    override fun initializeComponents(view: View) {

    }
}