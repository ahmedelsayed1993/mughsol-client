package com.aait.maghsoolclient.UI.Activities.Main.Client

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.maghsoolclient.Base.ParentActivity
import com.aait.maghsoolclient.Cart.AllCartViewModel
import com.aait.maghsoolclient.Cart.CartDataBase
import com.aait.maghsoolclient.Cart.ProviderModelOffline
import com.aait.maghsoolclient.Listeners.OnItemClickListener
import com.aait.maghsoolclient.Models.Model

import com.aait.maghsoolclient.Network.Client
import com.aait.maghsoolclient.Network.Service
import com.aait.maghsoolclient.R
import com.aait.maghsoolclient.UI.Activities.Auth.LoginActivity
import com.aait.maghsoolclient.UI.Controllers.CartProductAdapter
import com.aait.maghsoolclient.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CartProductsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_products
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var products:RecyclerView

    lateinit var order:Button
    lateinit var cancel:Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var cartProductsAdapter: CartProductAdapter
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()
    var providers = ArrayList<ProviderModelOffline>()
    var id = 0
    var tot = 0
    var value = 0
    var ta = 0
    var count = 0
    var price = 0
    var cart = ArrayList<Model>()

    var carts = ArrayList<Model>()
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        order = findViewById(R.id.order)
        cancel = findViewById(R.id.cancel)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        back.setOnClickListener { onBackPressed()
            finish()}



        products = findViewById(R.id.cats)

        providers.clear()
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        cartProductsAdapter = CartProductAdapter(mContext,ArrayList<ProviderModelOffline>(),R.layout.recycle_products)
        products.layoutManager = linearLayoutManager
        products.adapter = cartProductsAdapter
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        //allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)


        allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                cartModelOfflines = cartModels
                if(cartModels.size==0){
                }else {
                    for (i in 0..cartModels.size-1){
                        if (cartModels.get(i).provider_id==id) {
                            providers.add(cartModels.get(i))
                        }
                    }
                    Log.e("car", Gson().toJson(providers))
                    cartProductsAdapter.updateAll(providers.distinctBy { it.product_id })
                    for (i in 0..cartProductsAdapter.data.size-1){
                        value = value+(cartProductsAdapter.data.get(i).price!!.toInt()*cartProductsAdapter.data.get(i).count!!)
                        cart.add(Model(cartProductsAdapter.data.get(i).product_id,cartProductsAdapter.data.get(i).subcategory,cartProductsAdapter.data.get(i).price,cartProductsAdapter.data.get(i).count))
                    }
                    Log.e("valuee",value.toString())
                    Log.e("Model",Gson().toJson(cart))
                }
            }
        })

        cancel.setOnClickListener {
            allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
                override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
                    cartModelOfflines = cartModels
                    for (i in 0..cartModels.size-1){
                        if (cartModels.get(i).provider_id==id) {
                            allCartViewModel!!.deleteItem(cartModels.get(i))
                        }
                    }
                    Log.e("cart", Gson().toJson(cartModels))
                }
            })
        }
        order.setOnClickListener {
            if (user.loginStatus!!){
                val intent = Intent(this,CompleteCartOrderActivity::class.java)
                intent.putExtra("cart",cart)
                intent.putExtra("id",id)
                startActivity(intent)
            }
        }


    }


}


