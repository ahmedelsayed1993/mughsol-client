package com.aait.maghsoolclient.UI.Activities.Auth

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.*
import com.aait.maghsoolclient.Base.ParentActivity
import com.aait.maghsoolclient.Models.UserResponse
import com.aait.maghsoolclient.Network.Client
import com.aait.maghsoolclient.Network.Service
import com.aait.maghsoolclient.R
import com.aait.maghsoolclient.UI.Activities.Main.Client.MainActivity
import com.aait.maghsoolclient.Utils.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_login
    lateinit var phone: EditText
    lateinit var password: EditText
    lateinit var forgot_pass: TextView
    lateinit var login: Button
    lateinit var register: LinearLayout
    lateinit var skip:TextView
    var type = ""
    var deviceID = ""

    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        type = intent.getStringExtra("type")
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        skip = findViewById(R.id.skip)
        forgot_pass = findViewById(R.id.forgot)
        login = findViewById(R.id.login)
        register = findViewById(R.id.register)
        if (type.equals("user")){
            skip.visibility = View.VISIBLE
        }else{
            skip.visibility = View.GONE
        }
        register.setOnClickListener {
            if (type.equals("user")) {
                startActivity(Intent(this, RegisterActivity::class.java))
            }else{
                startActivity(Intent(this, RegisterDelegateActivity::class.java))
            }}
        forgot_pass.setOnClickListener { startActivity(Intent(this,ForgotPasswordActivity::class.java)) }
        login.setOnClickListener {
            if(CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkEditError(password,getString(R.string.enter_password))){
                return@setOnClickListener
            }else{
                Login()
            }
        }

    }

    fun Login(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Login(phone.text.toString()
                ,password.text.toString(),deviceID,"android",lang.appLanguage)?.enqueue(object:
                Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
                Log.e("error", Gson().toJson(t))
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data?.user_type!!.equals("user")) {
                            user.loginStatus = true
                            user.userData = response.body()?.data!!

                            val intent = Intent(this@LoginActivity, MainActivity::class.java)

                            startActivity(intent)
                            finish()

                        }else{

                        }
//                        val intent = Intent(this@LoginActivity, MainActivity::class.java)
//                        startActivity(intent)
//                        finish()
                    }else if (response.body()?.value.equals("2")){
                        val intent = Intent(this@LoginActivity,ActivateAccountActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}