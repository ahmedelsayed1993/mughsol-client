package com.aait.maghsoolclient.UI.Activities.Main.Client

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.aait.maghsoolclient.Base.ParentActivity
import com.aait.maghsoolclient.GPS.GPSTracker
import com.aait.maghsoolclient.GPS.GpsTrakerListener
import com.aait.maghsoolclient.Models.HomeResponse
import com.aait.maghsoolclient.Models.ProviderDetailsResponse
import com.aait.maghsoolclient.Models.ProvidersModel
import com.aait.maghsoolclient.Network.Client
import com.aait.maghsoolclient.Network.Service
import com.aait.maghsoolclient.R
import com.aait.maghsoolclient.Utils.CommonUtil
import com.aait.maghsoolclient.Utils.DialogUtil
import com.aait.maghsoolclient.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class ProviderDetailsActivity:ParentActivity(),GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_provider_details

    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var basket:ImageView
    lateinit var image:ImageView
    lateinit var name:TextView
    lateinit var address:TextView
    lateinit var distance:TextView
    lateinit var desc:TextView
    lateinit var reserve:Button
    private var mAlertDialog: AlertDialog? = null
    internal  var googleMap: GoogleMap?=null
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    var id = 0
    lateinit var providersModel: ProvidersModel
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        basket = findViewById(R.id.basket)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        address = findViewById(R.id.address)
        distance = findViewById(R.id.distance)
        desc = findViewById(R.id.desc)
        reserve = findViewById(R.id.reserve)
        back.setOnClickListener { onBackPressed()
        finish()}
        reserve.setOnClickListener {
            val intent = Intent(this,CategoriesActivity::class.java)
            intent.putExtra("provider",providersModel)
            startActivity(intent)
        }
        getLocationWithPermission(null)

    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true

    }
    fun getLocationWithPermission(cat:Int?) {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation(cat)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation(cat)
        }

    }

    internal fun getCurrentLocation(cat:Int?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                getData(gps.getLatitude().toString(),gps.getLongitude().toString())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext!!, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }
    fun getData(lat:String,lng:String){

         showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Provider(lang.appLanguage,id,lat,lng)?.enqueue(object:
                Callback<ProviderDetailsResponse> {
            override fun onFailure(call: Call<ProviderDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
            }

            override fun onResponse(
                    call: Call<ProviderDetailsResponse>,
                    response: Response<ProviderDetailsResponse>
            ) {
                hideProgressDialog()

                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        providersModel = response.body()?.data!!
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                        name.text = response.body()?.data?.name
                        address.text = response.body()?.data?.address
                        distance.text = response.body()?.data?.distance.toString()+"K"
                        desc.text = response.body()?.data?.desc
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}