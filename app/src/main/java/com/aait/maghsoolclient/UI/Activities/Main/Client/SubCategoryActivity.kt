package com.aait.maghsoolclient.UI.Activities.Main.Client

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.maghsoolclient.Base.ParentActivity
import com.aait.maghsoolclient.Cart.AddCartViewModel
import com.aait.maghsoolclient.Cart.AllCartViewModel
import com.aait.maghsoolclient.Cart.CartDataBase
import com.aait.maghsoolclient.Cart.ProviderModelOffline
import com.aait.maghsoolclient.Listeners.OnItemClickListener
import com.aait.maghsoolclient.Models.*
import com.aait.maghsoolclient.Network.Client
import com.aait.maghsoolclient.Network.Service
import com.aait.maghsoolclient.R
import com.aait.maghsoolclient.UI.Controllers.CategoryAdapter
import com.aait.maghsoolclient.UI.Controllers.ServiceAdapter
import com.aait.maghsoolclient.UI.Controllers.SubCategoryAdapter
import com.aait.maghsoolclient.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SubCategoryActivity : ParentActivity(), OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_subcategory
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var basket: ImageView
    lateinit var cats: RecyclerView
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var categoryAdapter: SubCategoryAdapter
    var categories = ArrayList<ListModel>()
    lateinit var services:RecyclerView
    lateinit var order: Button
    lateinit var cart: Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var serviceAdapter: ServiceAdapter
    var serviceModels = ArrayList<ServiceModel>()
    lateinit var servic:RelativeLayout
    lateinit var providersModel: ProvidersModel
    var cat = 0
    var service_id = 0
    var suncategory_id = 0
    var price = ""
    var quantity = 0
    var subCateory = ""
    var service_name = ""
    var image = ""
    override lateinit var addCartViewModel: AddCartViewModel
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartDataBase: CartDataBase
    internal lateinit var cartModels: LiveData<List<ProviderModelOffline>>
    internal var cartModelOfflines: List<ProviderModelOffline> = java.util.ArrayList()
    lateinit var cartModelOffline: ProviderModelOffline

    override fun initializeComponents() {
        providersModel = intent.getSerializableExtra("provider") as ProvidersModel
        cat = intent.getIntExtra("cat",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        basket = findViewById(R.id.basket)
        cats = findViewById(R.id.cats)
        title.text = getString(R.string.type)
        servic = findViewById(R.id.servic)
        gridLayoutManager = GridLayoutManager(mContext,2)
        categoryAdapter = SubCategoryAdapter(mContext,categories, R.layout.recycle_subcategory)
        categoryAdapter.setOnItemClickListener(this)
        cats.layoutManager = gridLayoutManager
        cats.adapter = categoryAdapter
        services = findViewById(R.id.services)
        order = findViewById(R.id.order)
        cart = findViewById(R.id.cart)
        cartDataBase = CartDataBase.getDataBase(mContext)
        addCartViewModel = ViewModelProviders.of(this).get(AddCartViewModel::class.java)
      //  allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        addCartViewModel = AddCartViewModel(application)
       // cartDataBase = CartDataBase.getDataBase(mContext!!)
       // allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
//        cartModels = allCartViewModel!!.allCart
//        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
//
//        allCartViewModel!!.allCart.observe(this, object : Observer<List<ProviderModelOffline>> {
//            override fun onChanged(@Nullable cartModels: List<ProviderModelOffline>) {
//                cartModelOfflines = cartModels
//
//                if(cartModels.size==0){
//                    //cart.visibility = View.GONE
//                }else {
//
//
//                    Log.e("cart", Gson().toJson(cartModels))
//
//
//                }
//
//
//            }
//        })
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        serviceAdapter = ServiceAdapter(mContext,serviceModels,R.layout.recycle_service)
        serviceAdapter.setOnItemClickListener(this)
        services.layoutManager = linearLayoutManager
        services.adapter = serviceAdapter
        back.setOnClickListener { onBackPressed()
            finish()}
        servic.setOnClickListener { servic.visibility = View.GONE }
        getData()
        order.setOnClickListener {
            if (quantity==0){
                CommonUtil.makeToast(mContext,getString(R.string.choose_quantity))
            }else {
                val intent = Intent(this, CompleteOrderActivity::class.java)
                intent.putExtra("count", quantity)
                intent.putExtra("service", service_id)
                intent.putExtra("price", price)
                intent.putExtra("sub", suncategory_id)
                intent.putExtra("id", providersModel.id)
                startActivity(intent)
                finish()
            }
            Log.e("count",quantity.toString())
            Log.e("service",service_id.toString())
            Log.e("price",price)
            Log.e("sub",suncategory_id.toString())
        }
        cart.setOnClickListener {
            if (quantity==0){
                CommonUtil.makeToast(mContext,getString(R.string.choose_quantity))
            }else{
                addCartViewModel.addItem(ProviderModelOffline(providersModel.id!!,
                providersModel.name!!,
                providersModel.avatar!!,
                providersModel.address!!,
                suncategory_id,
                subCateory,
                service_id,
                service_name,
                image,
                price,
                quantity
                ))

                servic.visibility = View.GONE
            }

        }
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SubCategory(lang.appLanguage,cat,providersModel.id!!)?.
        enqueue(object : Callback<ListResponse> {
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        categoryAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getData(id:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Services(lang.appLanguage,id)?.enqueue(object : Callback<ServiceResponse> {
            override fun onFailure(call: Call<ServiceResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<ServiceResponse>, response: Response<ServiceResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        servic.visibility = View.VISIBLE
                        serviceAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }


    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.lay){
            quantity=0
            service_id=0
            price = ""
            service_name = ""
            image = ""
            serviceAdapter.selected = position
            serviceModels.get(position).selected = true
            serviceAdapter.notifyDataSetChanged()
        }
        else if (view.id == R.id.add){
            quantity=0
            service_id=0
            price = ""
            service_name = ""
            image = ""
            serviceModels.get(position).count = serviceModels.get(position).count!!+1
            serviceAdapter.notifyDataSetChanged()
            quantity = serviceAdapter.data.get(position).count!!
            service_id = serviceAdapter.data.get(position).id!!
            price = serviceAdapter.data.get(position).price!!
            service_name = serviceAdapter.data.get(position).name!!
            image = serviceAdapter.data.get(position).image!!
        }else if (view.id == R.id.minus){
            quantity=0
            service_id=0
            price = ""
            service_name = ""
            image = ""
            if (serviceModels.get(position).count==0){

            }else {
                serviceModels.get(position).count = serviceModels.get(position).count!!-1
                serviceAdapter.notifyDataSetChanged()
                quantity = serviceAdapter.data.get(position).count!!
                service_id = serviceAdapter.data.get(position).id!!
                price = serviceAdapter.data.get(position).price!!
                service_name = serviceAdapter.data.get(position).name!!
                image = serviceAdapter.data.get(position).image!!
            }
        }else {
            suncategory_id = categories.get(position).subcategory_id!!
            subCateory = categories.get(position).name!!
            getData(categories.get(position).subcategory_id!!)
        }
    }
}