package com.aait.maghsoolclient.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.aait.maghsoolclient.Base.ParentRecyclerAdapter
import com.aait.maghsoolclient.Base.ParentRecyclerViewHolder
import com.aait.maghsoolclient.Models.InvoiceModel
import com.aait.maghsoolclient.Models.ProvidersModel
import com.aait.maghsoolclient.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class InvoiceAdapter (context: Context, data: MutableList<InvoiceModel>, layoutId: Int) :
        ParentRecyclerAdapter<InvoiceModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.type.text = listModel.subcategory
        viewHolder.service.text = listModel.service
        viewHolder.count.text = listModel.quantity
        viewHolder.price.text = listModel.total+mcontext.getString(R.string.rs)
    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var type=itemView.findViewById<TextView>(R.id.type)
        internal var service = itemView.findViewById<TextView>(R.id.service)
        internal var count = itemView.findViewById<TextView>(R.id.count)
        internal var price = itemView.findViewById<TextView>(R.id.price)


    }
}