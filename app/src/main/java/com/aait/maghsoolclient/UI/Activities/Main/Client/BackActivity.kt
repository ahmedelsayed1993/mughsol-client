package com.aait.maghsoolclient.UI.Activities.Main.Client

import android.content.Intent
import android.widget.TextView
import com.aait.maghsoolclient.Base.ParentActivity
import com.aait.maghsoolclient.R

class BackActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_order_done
lateinit var back:TextView
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
        finish()
    }
}