package com.aait.maghsoolclient.UI.Activities.Main.Client

import android.Manifest
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aait.maghsoolclient.Base.ParentActivity
import com.aait.maghsoolclient.Cart.AllCartViewModel
import com.aait.maghsoolclient.Cart.CartDataBase
import com.aait.maghsoolclient.Cart.ProviderModelOffline
import com.aait.maghsoolclient.GPS.GPSTracker
import com.aait.maghsoolclient.GPS.GpsTrakerListener
import com.aait.maghsoolclient.Models.Model
import com.aait.maghsoolclient.Models.OrderResponse
import com.aait.maghsoolclient.Network.Client
import com.aait.maghsoolclient.Network.Service
import com.aait.maghsoolclient.R
import com.aait.maghsoolclient.UI.Activities.LocationActivity
import com.aait.maghsoolclient.Utils.CommonUtil
import com.aait.maghsoolclient.Utils.DialogUtil
import com.aait.maghsoolclient.Utils.PermissionUtils
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_complete_order.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class CompleteOrderActivity : ParentActivity(), GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_complete_order
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var notification: ImageView
    lateinit var basket: ImageView
    lateinit var current: TextView
    lateinit var night: Switch
    lateinit var location: TextView
    lateinit var text: TextView
    lateinit var name: EditText
    lateinit var phone: EditText
    lateinit var date: TextView
    lateinit var time: TextView
    lateinit var next: Button
    var cart = ArrayList<Model>()
    var id = 0
    var lat = ""
    var lng = ""
    var result = ""
    var state = 0
    private var mAlertDialog: AlertDialog? = null
    internal  var googleMap: GoogleMap?=null
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var service = 0
    var count = 0
    var price = ""
    var sub = 0

    override fun initializeComponents() {
        cart.clear()
        service = intent.getIntExtra("service",0)
        count = intent.getIntExtra("count",0)
        price = intent.getStringExtra("price")
        sub = intent.getIntExtra("sub",0)
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        basket = findViewById(R.id.basket)
        current = findViewById(R.id.current)
        night = findViewById(R.id.night)
        location = findViewById(R.id.location)
        text = findViewById(R.id.text)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        date = findViewById(R.id.date)
        time = findViewById(R.id.time)
        next = findViewById(R.id.next)
        title.text = getString(R.string.confirm_order)
        phone.setText(user.userData.phone)
        name.setText(user.userData.name)
        getLocationWithPermission()
        cart.add(Model(service,sub,price,count))
        if (state==0){

            night.isChecked = true
            text.visibility = View.GONE
            location.visibility = View.GONE
            current.text = result
        }else{
            lat = ""
            lng = ""
            result = ""
            night.isChecked = false
            text.visibility = View.VISIBLE
            location.visibility = View.VISIBLE
            current.text = result
        }
        night.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                state = 0
                getLocationWithPermission()
                night.isChecked = true
                text.visibility = View.GONE
                location.visibility = View.GONE
                current.text = result
            }else{
                state = 1
                lat = ""
                lng = ""
                result = ""
                night.isChecked = false
                text.visibility = View.VISIBLE
                location.visibility = View.VISIBLE
                current.text = result
            }
        })
        date.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(mContext, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                var m = monthOfYear+1
                date.setText("" + dayOfMonth + "-" + m + "-" + year)
            }, year, month, day)
            dpd.datePicker.minDate = System.currentTimeMillis() - 1000
            dpd.show()
        }

        time.setOnClickListener {
            val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)
            val myTimeListener =
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        if (view.isShown) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                            myCalender.set(Calendar.MINUTE, minute)
                            var hour = hourOfDay
                            var minute = minute
                            // hour1 = hourOfDay.toString()
                            var am_pm = ""
                            val hours = if (hour < 10) "0" + hour else hour
                            val minutes = if (minute < 10) "0" + minute else minute
                            time.text = hours.toString()+":"+minutes.toString()
                        }
                    }
            val timePickerDialog = TimePickerDialog(
                    this,
                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                    myTimeListener,
                    hour,
                    minute,
                    false
            )
            timePickerDialog.setTitle(getString(R.string.time))
            timePickerDialog.window!!.setBackgroundDrawableResource(R.color.colorGray)
            timePickerDialog.show()
        }
        location.setOnClickListener {
            startActivityForResult(Intent(this, LocationActivity::class.java),1)
        }
        next.setOnClickListener {
            if (state==0){
                if (CommonUtil.checkTextError(current,getString(R.string.your_current_location))||
                        CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                        CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                        CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                        CommonUtil.checkTextError(date,getString(R.string.date))||
                        CommonUtil.checkTextError(time,getString(R.string.time))){
                    return@setOnClickListener
                }else{
                    AddOrder()
                }
            }else{
                if (CommonUtil.checkTextError(location,getString(R.string.another_location))||
                        CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                        CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                        CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                        CommonUtil.checkTextError(date,getString(R.string.date))||
                        CommonUtil.checkTextError(time,getString(R.string.time))){
                    return@setOnClickListener
                }else{
                    AddOrder()
                }
            }
        }

    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                lat = gps.getLatitude().toString()
                lng = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext!!, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(lat),
                            java.lang.Double.parseDouble(lng),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        current.text = result
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))
                    }

                } catch (e: IOException) {
                }
                //putMapMarker(gps.getLatitude(), gps.getLongitude())
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                location.text = result
            } else {
                result = ""
                lat = ""
                lng = ""
                location.text = ""
            }
        }
    }

    fun AddOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddOrder("Bearer"+user.userData.token,lang.appLanguage,
                id,result,lat,lng,name.text.toString(),phone.text.toString(),date.text.toString(),time.text.toString(), Gson().toJson(cart),notes.text.toString())
                ?.enqueue(object : Callback<OrderResponse> {
                    override fun onFailure(call: Call<OrderResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<OrderResponse>, response: Response<OrderResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                val intent = Intent(this@CompleteOrderActivity,InvoiceActivity::class.java)
                                intent.putExtra("id",response.body()?.data!!)
                                startActivity(intent)
                                finish()

                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}