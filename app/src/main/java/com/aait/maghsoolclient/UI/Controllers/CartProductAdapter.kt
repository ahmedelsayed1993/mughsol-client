package com.aait.maghsoolclient.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.maghsoolclient.Base.ParentRecyclerAdapter
import com.aait.maghsoolclient.Base.ParentRecyclerViewHolder
import com.aait.maghsoolclient.Cart.ProviderModelOffline
import com.aait.maghsoolclient.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class CartProductAdapter (context: Context, data: MutableList<ProviderModelOffline>, layoutId: Int) :
        ParentRecyclerAdapter<ProviderModelOffline>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.type!!.setText(questionModel.name)
        Glide.with(mcontext).load(questionModel.provider_image).into(viewHolder.image)
        viewHolder.count.text = questionModel.count.toString()
        viewHolder.price.text = questionModel.price+mcontext.getString(R.string.rs)
        viewHolder.service.text = questionModel.product_name


        // viewHolder.itemView.animation = mcontext.resources.
        // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
//        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_fall_down)
//        animation.setDuration(750)
//        viewHolder.itemView.startAnimation(animation)




    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {





        internal var type=itemView.findViewById<TextView>(R.id.type)
        internal var image = itemView.findViewById<CircleImageView>(R.id.image)
        internal var count = itemView.findViewById<TextView>(R.id.count)
        internal var service = itemView.findViewById<TextView>(R.id.service)
        internal var price = itemView.findViewById<TextView>(R.id.price)



    }
}