package com.aait.maghsoolclient.UI.Activities.Main.Client

import android.view.View
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.maghsoolclient.Base.ParentActivity
import com.aait.maghsoolclient.Listeners.OnItemClickListener
import com.aait.maghsoolclient.Models.ServiceModel
import com.aait.maghsoolclient.Models.ServiceResponse
import com.aait.maghsoolclient.Network.Client
import com.aait.maghsoolclient.Network.Service
import com.aait.maghsoolclient.R
import com.aait.maghsoolclient.UI.Controllers.ServiceAdapter
import com.aait.maghsoolclient.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ServicesActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_services
    lateinit var services:RecyclerView
    lateinit var order:Button
    lateinit var cart:Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var serviceAdapter:ServiceAdapter
    var serviceModels = ArrayList<ServiceModel>()
    var id = 0
    var cat = 0
    var subcat = 0
    var address = ""
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        cat = intent.getIntExtra("cat",0)
        subcat = intent.getIntExtra("sub",0)
        address = intent.getStringExtra("address")
        services = findViewById(R.id.services)
        order = findViewById(R.id.order)
        cart = findViewById(R.id.cart)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        serviceAdapter = ServiceAdapter(mContext,serviceModels,R.layout.recycle_service)
        serviceAdapter.setOnItemClickListener(this)
        services.layoutManager = linearLayoutManager
        services.adapter = serviceAdapter
        getData()
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Services(lang.appLanguage,subcat)?.enqueue(object : Callback<ServiceResponse> {
            override fun onFailure(call: Call<ServiceResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<ServiceResponse>, response: Response<ServiceResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        serviceAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.lay){
             serviceAdapter.selected = position
            serviceModels.get(position).selected = true
            serviceAdapter.notifyDataSetChanged()
        }
        else if (view.id == R.id.add){
            serviceModels.get(position).count = serviceModels.get(position).count!!+1
            serviceAdapter.notifyDataSetChanged()

        }else if (view.id == R.id.minus){
            if (serviceModels.get(position).count==0){

            }else {
                serviceModels.get(position).count = serviceModels.get(position).count!!-1
                serviceAdapter.notifyDataSetChanged()
            }
        }

    }
}