package com.aait.maghsoolclient.UI.Activities.Main.Client

import android.content.Intent
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.aait.maghsoolclient.Base.ParentActivity
import com.aait.maghsoolclient.R
import com.aait.maghsoolclient.UI.Activities.AppInfo.AboutAppActivity
import com.aait.maghsoolclient.UI.Activities.AppInfo.QuestionActivity
import com.aait.maghsoolclient.UI.Activities.AppInfo.TermsActivity
import com.aait.maghsoolclient.UI.Fragments.Client.HomeFragment
import com.aait.maghsoolclient.UI.Fragments.Client.OrdersFragment
import com.aait.maghsoolclient.UI.Fragments.Client.ProfileFragment
import com.aait.maghsoolclient.Utils.CommonUtil
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_main
    lateinit var menu:ImageView
    lateinit var search:ImageView
    lateinit var notification:ImageView
    lateinit var basket:ImageView
    lateinit var home:ImageView
    lateinit var orders:ImageView
    lateinit var profile:ImageView
    private var fragmentManager: FragmentManager? = null
    var selected = 1
    private var transaction: FragmentTransaction? = null
    internal lateinit var homeFragment: HomeFragment
    internal lateinit var ordersFragment: OrdersFragment
    internal lateinit var profileFragment: ProfileFragment
    lateinit var main:LinearLayout
    lateinit var mydata:LinearLayout
    lateinit var myorders:LinearLayout
    lateinit var chats:LinearLayout
    lateinit var about_app:LinearLayout
    lateinit var questions:LinearLayout
    lateinit var terms:LinearLayout
    lateinit var contact_us:LinearLayout
    lateinit var complaints:LinearLayout
    lateinit var settings:LinearLayout
    lateinit var logout:LinearLayout
    lateinit var text:TextView
    lateinit var image:CircleImageView

    override fun initializeComponents() {
        menu = findViewById(R.id.menu)
        search = findViewById(R.id.search)
        notification = findViewById(R.id.notification)
        basket = findViewById(R.id.basket)
        home = findViewById(R.id.home)
        orders = findViewById(R.id.orders)
        profile = findViewById(R.id.profile)
        homeFragment = HomeFragment.newInstance()
        ordersFragment = OrdersFragment.newInstance()
        profileFragment = ProfileFragment.newInstance()
        fragmentManager = supportFragmentManager
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container,homeFragment)
        transaction!!.add(R.id.home_fragment_container,ordersFragment)
        transaction!!.add(R.id.home_fragment_container,profileFragment)
        transaction!!.commit()
        basket.setOnClickListener { startActivity(Intent(this,BasketActivity::class.java)) }
        showhome()
        home.setOnClickListener { showhome() }
        orders.setOnClickListener { showOrders() }
        profile.setOnClickListener { showprofile() }
        sideMenu()
    }

    fun showhome(){
        selected = 1
        home.setImageResource(R.mipmap.home_active)
        orders.setImageResource(R.mipmap.terms)
        profile.setImageResource(R.mipmap.profile)
        search.visibility = View.VISIBLE
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, homeFragment)
        transaction!!.commit()
    }
    fun showOrders(){
        selected = 1
        home.setImageResource(R.mipmap.home)
        orders.setImageResource(R.mipmap.terms_active)
        profile.setImageResource(R.mipmap.profile)
        search.visibility = View.GONE
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, ordersFragment)
        transaction!!.commit()
    }
    fun showprofile(){
        selected = 1
        home.setImageResource(R.mipmap.home)
        orders.setImageResource(R.mipmap.terms)
        profile.setImageResource(R.mipmap.profile_active)
        search.visibility = View.GONE
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, profileFragment)
        transaction!!.commit()
    }
    fun sideMenu(){
        drawer_layout.useCustomBehavior(Gravity.START)
        drawer_layout.useCustomBehavior(Gravity.END)
        drawer_layout.setRadius(Gravity.START, 150f)
        drawer_layout.setRadius(Gravity.END, 150f)
        drawer_layout.setViewScale(Gravity.START, 1f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewScale(Gravity.END, 1f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewElevation(Gravity.START, 0f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewElevation(Gravity.END, 0f) //set main view elevation when drawer open (dimension)
//        drawer_layout.setViewScrimColor(Gravity.START, Color.) //set drawer overlay coloe (color)
//        drawer_layout.setViewScrimColor(Gravity.END, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setDrawerElevation(Gravity.START, 10f) //set drawer elevation (dimension)
        drawer_layout.setDrawerElevation(Gravity.END, 10f) //set drawer elevation (dimension)
        drawer_layout.setContrastThreshold(2f) //set maximum of contrast ratio between white text and background color.
        drawer_layout.setRadius(Gravity.START, 0f) //set end container's corner radius (dimension)

        drawer_layout.setRadius(Gravity.END, 0f)

        menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }

        image = drawer_layout.findViewById(R.id.image)
        main = drawer_layout.findViewById(R.id.main)
        mydata = drawer_layout.findViewById(R.id.mydata)
        settings = drawer_layout.findViewById(R.id.settings)
        myorders = drawer_layout.findViewById(R.id.myorders)
       chats = drawer_layout.findViewById(R.id.chats)
        terms = drawer_layout.findViewById(R.id.terms)
        about_app = drawer_layout.findViewById(R.id.about_app)
        contact_us = drawer_layout.findViewById(R.id.contact_us)
        complaints = drawer_layout.findViewById(R.id.complaints)
        questions = drawer_layout.findViewById(R.id.questions)

        logout = drawer_layout.findViewById(R.id.logout)
        terms.setOnClickListener { startActivity(Intent(this,TermsActivity::class.java)) }
        questions.setOnClickListener { startActivity(Intent(this,QuestionActivity::class.java)) }
        profile.setOnClickListener {
            showprofile()
           }


        about_app.setOnClickListener { startActivity(Intent(this,AboutAppActivity::class.java)) }
        contact_us.setOnClickListener {  }

        complaints.setOnClickListener {  }
        settings.setOnClickListener { }
        main.setOnClickListener { startActivity(Intent(this,MainActivity::class.java)) }

    }
}