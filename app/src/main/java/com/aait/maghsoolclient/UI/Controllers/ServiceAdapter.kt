package com.aait.maghsoolclient.UI.Controllers

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.maghsoolclient.Base.ParentRecyclerAdapter
import com.aait.maghsoolclient.Base.ParentRecyclerViewHolder
import com.aait.maghsoolclient.Models.ProvidersModel
import com.aait.maghsoolclient.Models.ServiceModel
import com.aait.maghsoolclient.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class ServiceAdapter (context: Context, data: MutableList<ServiceModel>, layoutId: Int) :
        ParentRecyclerAdapter<ServiceModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var selected :Int = 0
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        Glide.with(mcontext).asBitmap().load(listModel.image).into(viewHolder.image)
        listModel.selected = (selected==position)
        viewHolder.name.text = listModel.name
        viewHolder.price.text = listModel.price+mcontext.getString(R.string.rs)
        if (listModel.selected!!){
            viewHolder.image.foreground = mcontext.getDrawable(R.drawable.transparent_circle)
            viewHolder.checked.setImageResource(R.mipmap.selectedactive)
            viewHolder.count.text = listModel.count.toString()
            viewHolder.add.isEnabled = true
            viewHolder.minus.isEnabled = true
        }else {
            viewHolder.image.foreground = mcontext.getDrawable(R.drawable.trans_circle)
            viewHolder.checked.setImageResource(R.mipmap.selected)
            viewHolder.count.text = "0"
            viewHolder.add.isEnabled = false
            viewHolder.minus.isEnabled = false
        }
        viewHolder.add.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
        viewHolder.lay.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
        viewHolder.minus.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var image=itemView.findViewById<CircleImageView>(R.id.image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var price = itemView.findViewById<TextView>(R.id.price)
        internal var count = itemView.findViewById<TextView>(R.id.count)
        internal var add  = itemView.findViewById<ImageView>(R.id.add)
        internal var minus = itemView.findViewById<ImageView>(R.id.minus)
        internal var select = itemView.findViewById<ImageView>(R.id.selected)
        internal var lay = itemView.findViewById<RelativeLayout>(R.id.lay)
        internal var checked = itemView.findViewById<ImageView>(R.id.checked)
    }
}