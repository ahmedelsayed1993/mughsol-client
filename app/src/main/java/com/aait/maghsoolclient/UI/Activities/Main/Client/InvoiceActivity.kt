package com.aait.maghsoolclient.UI.Activities.Main.Client

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.maghsoolclient.Base.ParentActivity
import com.aait.maghsoolclient.Models.InvoiceModel
import com.aait.maghsoolclient.Models.InvoiceResponse
import com.aait.maghsoolclient.Models.TermsResponse
import com.aait.maghsoolclient.Network.Client
import com.aait.maghsoolclient.Network.Service
import com.aait.maghsoolclient.R
import com.aait.maghsoolclient.UI.Controllers.InvoiceAdapter
import com.aait.maghsoolclient.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InvoiceActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_invoice
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var basket:ImageView
    lateinit var rv_recycle:RecyclerView
    lateinit var delivery:TextView
    lateinit var order_num:TextView
    lateinit var total:TextView
    lateinit var confirm:Button
    lateinit var delete:Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var invoiceAdapter: InvoiceAdapter
    var invoices = ArrayList<InvoiceModel>()
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        basket = findViewById(R.id.basket)
        rv_recycle = findViewById(R.id.rv_recycle)
        delivery = findViewById(R.id.delivery)
        order_num = findViewById(R.id.order_num)
        total = findViewById(R.id.total)
        confirm = findViewById(R.id.confirm)
        delete = findViewById(R.id.delete)
        title.text = getString(R.string.invoice)
        basket.setOnClickListener { startActivity(Intent(this,BasketActivity::class.java))
        finish()}
        back.setOnClickListener { onBackPressed()
        finish()}
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        invoiceAdapter = InvoiceAdapter(mContext,invoices,R.layout.recycle_invoice)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = invoiceAdapter
        getData()
        confirm.setOnClickListener { startActivity(Intent(this,BackActivity::class.java))
        finish()}
        delete.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.DELETE("Bearer "+user.userData.token,lang.appLanguage,id,"delete")
                    ?.enqueue(object : Callback<TermsResponse> {
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.data!!)
                                    startActivity(Intent(this@InvoiceActivity,MainActivity::class.java))
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Invoice("Bearer "+user.userData.token,lang.appLanguage,id,"current")
                ?.enqueue(object : Callback<InvoiceResponse> {
                    override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<InvoiceResponse>, response: Response<InvoiceResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                invoiceAdapter.updateAll(response.body()?.data!!)
                                delivery.text = response.body()?.delivery_price+getString(R.string.rs)
                                order_num.text = response.body()?.order.toString()
                                total.text = getString(R.string.total)+response.body()?.total_order+getString(R.string.rs)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}