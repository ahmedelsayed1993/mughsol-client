package com.aait.maghsoolclient.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import com.aait.maghsoolclient.Base.ParentRecyclerAdapter
import com.aait.maghsoolclient.Base.ParentRecyclerViewHolder
import com.aait.maghsoolclient.Models.QuestionModel
import com.aait.maghsoolclient.R

import com.bumptech.glide.Glide

class QuestionsAdapter (context: Context, data: MutableList<QuestionModel>, layoutId: Int) :
    ParentRecyclerAdapter<QuestionModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.question!!.setText(questionModel.question)
        viewHolder.answer!!.setText(questionModel.answer)







    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var question=itemView.findViewById<TextView>(R.id.question)
        internal var answer = itemView.findViewById<TextView>(R.id.answer)



    }
}