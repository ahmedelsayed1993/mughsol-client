package com.aait.maghsoolclient.UI.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatDelegate
import com.aait.maghsoolclient.Base.ParentActivity
import com.aait.maghsoolclient.R
import com.aait.maghsoolclient.UI.Activities.Main.Client.MainActivity

class SplashActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_splash
    var isSplashFinishid = false
    override fun initializeComponents() {

        Handler().postDelayed({
            // logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true
                if (user.loginStatus==true){
                    var intent = Intent(this@SplashActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()


                }else {
                    var intent = Intent(this@SplashActivity, ChooseLangActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }, 2100)
        }, 1800)
    }

}