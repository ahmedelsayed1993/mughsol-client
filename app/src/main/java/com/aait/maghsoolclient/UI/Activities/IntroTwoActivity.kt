package com.aait.maghsoolclient.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.aait.maghsoolclient.Base.ParentActivity
import com.aait.maghsoolclient.R
import com.aait.maghsoolclient.UI.Activities.Auth.LoginActivity

class IntroTwoActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_intro_two
    lateinit var title: TextView
    lateinit var desc: TextView
    lateinit var next: ImageView
    lateinit var skip: TextView
    var type = ""
    override fun initializeComponents() {
        type = intent.getStringExtra("type")
        title = findViewById(R.id.title)
        desc = findViewById(R.id.desc)
        next = findViewById(R.id.next)
        skip = findViewById(R.id.skip)
        next.setOnClickListener { val intent = Intent(this,LoginActivity::class.java)
            intent.putExtra("type",type)
            startActivity(intent)
            finish()
        }
        skip.setOnClickListener { val intent = Intent(this, LoginActivity::class.java)
            intent.putExtra("type",type)
            startActivity(intent)
            finish()
        }

    }
}