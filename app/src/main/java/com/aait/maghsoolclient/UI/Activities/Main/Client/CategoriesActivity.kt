package com.aait.maghsoolclient.UI.Activities.Main.Client

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.maghsoolclient.Base.ParentActivity
import com.aait.maghsoolclient.Listeners.OnItemClickListener
import com.aait.maghsoolclient.Models.ListModel
import com.aait.maghsoolclient.Models.ListResponse
import com.aait.maghsoolclient.Models.ProvidersModel
import com.aait.maghsoolclient.Network.Client
import com.aait.maghsoolclient.Network.Service
import com.aait.maghsoolclient.R
import com.aait.maghsoolclient.UI.Controllers.CategoryAdapter
import com.aait.maghsoolclient.Utils.CommonUtil
import com.google.android.gms.common.api.Api
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CategoriesActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_categories
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var basket:ImageView
    lateinit var cats:RecyclerView
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var categoryAdapter: CategoryAdapter
    var categories = ArrayList<ListModel>()
    lateinit var providersModel: ProvidersModel
    override fun initializeComponents() {
        providersModel = intent.getSerializableExtra("provider") as ProvidersModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        basket = findViewById(R.id.basket)
        cats = findViewById(R.id.cats)
        gridLayoutManager = GridLayoutManager(mContext,2)
        categoryAdapter = CategoryAdapter(mContext,categories,R.layout.recycle_category)
        categoryAdapter.setOnItemClickListener(this)
        cats.layoutManager = gridLayoutManager
        cats.adapter = categoryAdapter
        back.setOnClickListener { onBackPressed()
        finish()}
        getData()
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Categories(lang.appLanguage)?.
                enqueue(object : Callback<ListResponse>{
                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                categoryAdapter.updateAll(response.body()?.data!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this,SubCategoryActivity::class.java)
        intent.putExtra("provider",providersModel)
        intent.putExtra("cat",categories.get(position).id)

        startActivity(intent)
    }
}