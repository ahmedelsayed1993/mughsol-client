package com.aait.maghsoolclient.UI.Activities.Auth

import com.aait.maghsoolclient.Base.ParentActivity
import com.aait.maghsoolclient.R

class RegisterDelegateActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_register_delegate

    override fun initializeComponents() {

    }
}