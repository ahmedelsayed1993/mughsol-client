package com.aait.maghsoolclient.UI.Fragments.Client

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.maghsoolclient.Base.BaseFragment
import com.aait.maghsoolclient.GPS.GPSTracker
import com.aait.maghsoolclient.GPS.GpsTrakerListener
import com.aait.maghsoolclient.Listeners.OnItemClickListener
import com.aait.maghsoolclient.Models.HomeResponse
import com.aait.maghsoolclient.Models.ProvidersModel
import com.aait.maghsoolclient.Network.Client
import com.aait.maghsoolclient.Network.Service
import com.aait.maghsoolclient.R
import com.aait.maghsoolclient.UI.Activities.Main.Client.ProviderDetailsActivity
import com.aait.maghsoolclient.UI.Controllers.ProviderAdapter
import com.aait.maghsoolclient.UI.Controllers.SlidersAdapter
import com.aait.maghsoolclient.Utils.CommonUtil
import com.aait.maghsoolclient.Utils.DialogUtil
import com.aait.maghsoolclient.Utils.PermissionUtils
import com.github.islamkhsh.CardSliderViewPager
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import me.relex.circleindicator.CircleIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment:BaseFragment(),OnItemClickListener,GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.fragment_home
    companion object {
        fun newInstance(): HomeFragment {
            val args = Bundle()
            val fragment = HomeFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var viewPager: CardSliderViewPager
    lateinit var indicator: CircleIndicator
    lateinit var providrs:RecyclerView
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var providerAdapter: ProviderAdapter
    var providersModels = ArrayList<ProvidersModel>()
    private var mAlertDialog: AlertDialog? = null
    internal  var googleMap: GoogleMap?=null
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    override fun initializeComponents(view: View) {
        viewPager = view.findViewById(R.id.viewPager)
        indicator = view.findViewById(R.id.indicator)
        providrs = view.findViewById(R.id.providers)
        gridLayoutManager = GridLayoutManager(mContext!!,2)
        providerAdapter = ProviderAdapter(mContext!!,providersModels,R.layout.recycle_home)
        providerAdapter.setOnItemClickListener(this)
        providrs.layoutManager = gridLayoutManager
        providrs.adapter = providerAdapter
        getLocationWithPermission(null)
    }

    override fun onResume() {
        super.onResume()
        getLocationWithPermission(null)
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(activity,ProviderDetailsActivity::class.java)
        intent.putExtra("id",providersModels.get(position).id)
        startActivity(intent)
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true

    }
    fun getLocationWithPermission(cat:Int?) {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation(cat)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation(cat)
        }

    }

    internal fun getCurrentLocation(cat:Int?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                getData(gps.getLatitude().toString(),gps.getLongitude().toString())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext!!, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }
    fun getData(lat:String,lng:String){

        // showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Home(lang.appLanguage,lat,lng,null)?.enqueue(object:
                Callback<HomeResponse> {
            override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
            }

            override fun onResponse(
                    call: Call<HomeResponse>,
                    response: Response<HomeResponse>
            ) {
                hideProgressDialog()

                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        initSliderAds(response.body()?.banners!!)

                            providerAdapter.updateAll(response.body()?.data!!)


                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun initSliderAds(list: java.util.ArrayList<String>){
        if(list.isEmpty()){
            viewPager.visibility=View.GONE
            indicator.visibility = View.GONE
        }
        else{
            viewPager.visibility=View.VISIBLE
            indicator.visibility = View.VISIBLE
            viewPager.adapter= SlidersAdapter(mContext!!,list)
            indicator.setViewPager(viewPager)
        }
    }
}