package com.aait.maghsoolclient.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.aait.maghsoolclient.Base.ParentRecyclerAdapter
import com.aait.maghsoolclient.Base.ParentRecyclerViewHolder
import com.aait.maghsoolclient.Models.ListModel
import com.aait.maghsoolclient.Models.QuestionModel
import com.aait.maghsoolclient.R

class CategoryAdapter (context: Context, data: MutableList<ListModel>, layoutId: Int) :
        ParentRecyclerAdapter<ListModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.cat!!.setText(questionModel.name)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var cat=itemView.findViewById<TextView>(R.id.cat)


    }
}