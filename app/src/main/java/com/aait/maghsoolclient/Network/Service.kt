package com.aait.maghsoolclient.Network

import com.aait.maghsoolclient.Models.*
import retrofit2.Call
import retrofit2.http.*

interface Service {
    @POST("about")
    fun About(@Header("lang") lang: String): Call<TermsResponse>

    @POST("terms")
    fun Terms(@Header("lang") lang: String): Call<TermsResponse>
    @POST("questions")
    fun Questions(@Header("lang") lang: String): Call<QuestionsResponse>
    @FormUrlEncoded
    @POST("sign-up-user")
    fun SignUp(@Field("name") name:String,
               @Field("phone") phone:String,
               @Field("address") address:String,
               @Field("lat") lat:String,
               @Field("lng") lng:String,
               @Field("password") password:String,
               @Field("device_id") device_id:String,
               @Field("device_type") device_type:String,
               @Header("lang") lang:String): Call<UserResponse>

    @FormUrlEncoded
    @POST("check-code")
    fun CheckCode(@Header("Authorization") Authorization:String,
                  @Field("code") code:String,
                  @Header("lang") lang: String):Call<UserResponse>

    @POST("resend-code")
    fun Resend(@Header("Authorization") Authorization:String,
               @Header("lang") lang: String):Call<UserResponse>

    @FormUrlEncoded
    @POST("sign-in")
    fun Login(@Field("phone") phone:String,
              @Field("password") password:String,
              @Field("device_id") device_id:String,
              @Field("device_type") device_type:String,
              @Header("lang") lang: String):Call<UserResponse>

    @FormUrlEncoded
    @POST("forget-password")
    fun ForGot(@Field("phone") phone:String,
               @Header("lang") lang:String):Call<UserResponse>

    @FormUrlEncoded
    @POST("update-password")
    fun NewPass(@Header("Authorization") Authorization:String,
                @Field("password") password:String,
                @Field("code") code:String,
                @Header("lang") lang: String):Call<UserResponse>

    @POST("main-user")
    fun Home(@Header("lang") lang: String,
             @Query("lat") lat:String,
             @Query("lng") lng:String,
             @Query("text_serach") text_serach:String?):Call<HomeResponse>
    @POST("details-provider")
    fun Provider(@Header("lang") lang: String,
                 @Query("provider_id") provider_id:Int,
             @Query("lat") lat:String,
             @Query("lng") lng:String):Call<ProviderDetailsResponse>

    @POST("categories")
    fun Categories(@Header("lang") lang: String):Call<ListResponse>

    @POST("subcategories-provider")
    fun SubCategory(@Header("lang") lang: String,
    @Query("category_id") category_id:Int,
    @Query("provider_id") provider_id:Int):Call<ListResponse>

    @POST("services-subcategory")
    fun Services(@Header("lang") lang: String,
    @Query("subcategory_id") subcategory_id:Int):Call<ServiceResponse>

    @POST("add-order")
    fun AddOrder(@Header("Authorization") Authorization:String,
                 @Header("lang") lang: String,
                 @Query("provider_id") provider_id:Int,
                 @Query("address") address:String,
                 @Query("lat") lat:String,
                 @Query("lng") lng:String,
                 @Query("name") name:String,
                 @Query("phone") phone:String,
                 @Query("date") date:String,
                 @Query("time") time:String,
                 @Query("carts") carts:String,
                 @Query("notes") notes:String):Call<OrderResponse>

    @POST("details-invoice")
    fun Invoice(@Header("Authorization") Authorization:String,
                @Header("lang") lang: String,
                @Query("order_id") order_id:Int,
                @Query("action") action:String):Call<InvoiceResponse>
    @POST("details-invoice")
    fun DELETE(@Header("Authorization") Authorization:String,
                @Header("lang") lang: String,
                @Query("order_id") order_id:Int,
                @Query("action") action:String):Call<TermsResponse>
}