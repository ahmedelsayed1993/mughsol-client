package com.aait.maghsoolclient.Listeners

import android.view.View

interface OnItemClickListener {
    fun onItemClick(view: View, position: Int)
}
