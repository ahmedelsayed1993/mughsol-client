package com.aait.maghsoolclient.Models

import java.io.Serializable

class ProvidersModel:Serializable {
    var id:Int?=null
    var avatar:String?=null
    var name:String?=null
    var address:String?=null
    var distance:Int?=null
    var desc:String?=null
}