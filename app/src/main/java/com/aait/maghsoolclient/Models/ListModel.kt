package com.aait.maghsoolclient.Models

import java.io.Serializable

class ListModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var image:String?=null
    var subcategory_id:Int?=null
}