package com.aait.maghsoolclient.Models

import java.io.Serializable

class InvoiceResponse:BaseResponse(),Serializable {
    var data:ArrayList<InvoiceModel>?=null
    var delivery_price:String?=null
    var order:Int?=null
    var total_order:String?=null
}