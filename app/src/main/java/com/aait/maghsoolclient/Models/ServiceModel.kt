package com.aait.maghsoolclient.Models

import java.io.Serializable

class ServiceModel:Serializable {
    var id:Int?=null
    var image:String?=null
    var name:String?=null
    var price:String?=null
    var count:Int?=null
    var selected:Boolean?=null
}