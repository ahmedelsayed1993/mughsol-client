package com.aait.maghsoolclient.Models

import java.io.Serializable

class Model:Serializable {
    var service_id:Int?=null
    var subcategory_id:Int?=null
    var price:String?=null
    var quantity:Int?=null

    constructor(service_id: Int?, subcategory_id: Int?, price: String?, quantity: Int?) {
        this.service_id = service_id
        this.subcategory_id = subcategory_id
        this.price = price
        this.quantity = quantity
    }
}