package com.aait.maghsoolclient.Models

import java.io.Serializable

class InvoiceModel:Serializable {
    var id:Int?=null
    var subcategory:String?=null
    var service:String?=null
    var quantity:String?=null
    var price:String?=null
    var total:String?=null

}