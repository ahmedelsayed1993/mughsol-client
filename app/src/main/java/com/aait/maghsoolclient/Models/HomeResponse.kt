package com.aait.maghsoolclient.Models

import java.io.Serializable

class HomeResponse:BaseResponse(),Serializable {
    var banners:ArrayList<String>?=null
    var data:ArrayList<ProvidersModel>?=null
}